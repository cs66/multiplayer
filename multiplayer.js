var mqtt = new Paho.Client('tw.igs.farm',8083,'cid'+Math.random());
var topic = 'awh';
$(function(){
  mqtt.connect({
    useSSL: true,
    onSuccess:function(){
      $('#connection-status').text('success'); 
      mqtt.subscribe(topic);
    },
    onFailure:function(d){
      $('#connection-status').text('failed');
    }
  });
  mqtt.onMessageArrived = function(d){
    $('#inmsg').text(d.payloadString);
  }

  $('#send').click(function(){
    mqtt.publish(topic,$('#outmsg').val());
  });
})
